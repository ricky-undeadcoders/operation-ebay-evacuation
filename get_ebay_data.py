import requests
import shutil
import json
import re
import os
import logging
import sys
import pendulum
import html
from bs4 import BeautifulSoup
from glob import glob

logger = logging.getLogger("ebayer")
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.FileHandler(f"log_{pendulum.now()}"))
logger.addHandler(logging.StreamHandler(sys.stdout))

here = os.path.dirname(__file__)


class OperationEbayEvacuation:

    def __init__(self, download_images=False):
        logger.info("Initializing")
        self.download_images = download_images
        self.input_dir = os.path.join(here, "input")
        self.output_dir = os.path.join(here, "output")
        self.draft_list_dir = os.path.join(here, "draft_lists")
        self.item_map = None

        if not os.path.exists(self.output_dir):
            os.mkdir(self.output_dir)

    def run(self):
        logger.info("And I didn't know where I was going but I was RUNNING")
        for file in glob(os.path.join(self.input_dir, "*")):
            logger.info(f"=========================================")
            logger.info(f"Looking at this file: {file}")
            output_file_name = os.path.basename(file).replace(".html", "").replace(".htm", "")
            if os.path.exists(os.path.join(self.output_dir, output_file_name)):
                logger.info("File exists, skipping")
                continue
            logger.info(f"Going to save our output to this file: {output_file_name}")
            try:
                self.extract_data_and_images_from_html_file(input_file=file,
                                                            output_file_dir=os.path.join(self.output_dir,
                                                                                         output_file_name))
            except Exception as e:
                logger.error(f"Fatal error attempting to extract from {file}")
                logger.exception(e)

        logger.info("Found our files, now we're going to attempt to match them to their SKUs")
        self.add_skus_into_item_json()
        self.convert_json_to_csv()

    def extract_data_and_images_from_html_file(self, input_file, output_file_dir):
        with open(input_file) as infile:
            content = infile.read()
        if not os.path.exists(output_file_dir):
            os.mkdir(output_file_dir)
        soup = BeautifulSoup(content, 'html.parser')

        item = {}

        """
        PATTERN 1
        <input id="editpane_whatever" value="data we care about">
        """
        fields_that_match_pattern_one = [("Title", "title"), ("SubTitle", "subtitle")]
        for field_name, field_id in fields_that_match_pattern_one:
            try:
                item[field_name] = soup.find("input", {"id": f"editpane_{field_id}"}).attrs["value"]
            except Exception as e:
                logger.error(f"failed pulling item title: {e}")

        """
        PATTERN 2
        <span id="labelcat1">data we care about</span>
        """
        fields_that_match_pattern_two = [("Category", "cat")]
        for field_name, field_id in fields_that_match_pattern_two:
            try:
                item[field_name] = soup.find("span", {"id": f"label{field_id}1"}).contents[0]
            except Exception as e:
                logger.error(f"failed pulling item {field_name}: {e}")

        """
        PATTERN 3
        <input id="upc" value="data we care about">
        """
        fields_that_match_pattern_three = [("UPC", "upc"),
                                           ("Buy It Now Price", "binPrice"),
                                           ("Item Description", "description"),
                                           ("Shipping Cost", "shipFee1")]
        for field_name, field_id in fields_that_match_pattern_three:
            try:
                if field_id == "description":
                    item_description = soup.find("input", {"id": field_id}).attrs['value'].replace("\n", "").replace(",", "")
                    item["Item Description"] = html.unescape(item_description)
                else:
                    item[field_name] = soup.find("input", {"id": field_id}).attrs['value']
            except Exception as e:
                logger.error(f"failed pulling item {field_name}: {e}")

        """
        PATTERN 4
        <select id="itemCondition"><option value="3000" selected="selected">Data we care about</option>
        """
        fields_that_match_pattern_four = [("Item Condition", "itemCondition")]
        for field_name, field_id in fields_that_match_pattern_four:
            try:
                for thing in soup.find("select", {"id": field_id}).children:
                    # print(vars(thing))
                    if thing.attrs.get("selected") == "selected":
                        item[field_name] = thing.contents[0]
            except Exception as e:
                logger.error(f"failed pulling item {field_name}: {e}")

        """
        PATTERN 5
        <textarea id="editpane_condDesc">Data we care about</textarea>
        """
        fields_that_match_pattern_five = [("Condition Description", "condDesc")]
        for field_name, field_id in fields_that_match_pattern_five:
            try:
                item[field_name] = soup.find("textarea", {"id": f"editpane_{field_id}"}).contents[0]
            except Exception as e:
                logger.error(f"failed pulling item {field_name}: {e}")

        """
        PATTERN 6
        <input id="List.Item.ItemSpecific[Brand]" value="data we care about">
        """
        fields = ['Brand', 'MPN', 'Model', 'Price']

        for field in fields:
            input_field = soup.find("input", {"id": f"Listing.Item.ItemSpecific[{field}]"})
            if input_field is not None:
                item[field] = input_field.attrs['value']

        """
        PATTERN 7
        <select id="shipService1"><optgroup><option selected="selected">Data we care about</option>
        """
        fields_that_match_pattern_four = [("Shipping Type", "shipService1")]
        for field_name, field_id in fields_that_match_pattern_four:
            try:
                souper = soup.find("select", {"id": field_id})
                item[field_name] = souper.find("option", {"selected": "selected"}).contents[0]
            except Exception as e:
                logger.error(f"failed pulling item {field_name}: {e}")

        logger.debug("The fields that we found for this item:")
        for field, value in item.items():
            logger.debug(f"{field}:\t{value}")

        logger.info("Writing output item.json file")
        with open(os.path.join(output_file_dir, "item.json"), "w") as outfile:
            outfile.write(json.dumps(item))

        if self.download_images is True:
            # TODO: get the logging here in order once you can test
            matches = re.findall('(https://i.ebayimg.com/.*)', content)
            print(matches)
            print(matches[0])
            for i, match in enumerate(matches[0].split(',')):
                print(i, match)
                try:
                    print(f"Attempting to download {match}")
                    url = match.replace('"url":', '').replace('"', '').replace('}', '').replace(']', '').replace(
                        '12.JPG',
                        '57.JPG')
                    if url[0:4] != "http":
                        print(f"Skipping!")
                        continue
                    print(f"The URL: {url}")
                    local_filename = os.path.join(output_file_dir, f"{i}.jpg")
                    with requests.get(url, stream=True) as r:
                        with open(local_filename, 'wb') as f:
                            shutil.copyfileobj(r.raw, f)
                except Exception as e:
                    print(f"Failed to download image {match}")
                    print(f"{e}")

    def get_sku_and_title_mapping(self):
        logger.info("Creating sku and title mapping")
        item_map = {}
        missed = 0
        for file in glob(os.path.join(self.draft_list_dir, "*.html")):
            logger.info(f"Checking file {file}")
            with open(file, encoding="utf8") as infile:
                content = infile.read()
            soup = BeautifulSoup(content, 'html.parser')
            sku_tds = soup.findAll('td', {"class": "shui-dt-column__listingSKU"})
            logger.info("Looping through the items in this file")
            for sku_td in sku_tds:
                try:
                    for child in sku_td.parent.children:
                        try:
                            if child.attrs['class'][0] == "shui-dt-column__title":
                                title_td = child
                                break
                        except:
                            pass
                    sku = sku_td.contents[0].contents[0].find("div", {"class": None}).contents[0]
                    title = title_td.contents[0].contents[0].find("div", {"class": None}).contents[0]
                    item_map[title] = sku
                    logger.debug(f"We found a mapping! SKU: {sku}\tTitle: '{title}'")

                except Exception as e:
                    logger.error(f"Failed extracting sku_td: {sku_td}")
                    logger.exception(e)
                    missed += 1

        logger.info(f"The number of skus we failed to match to a title: {missed}")
        self.item_map = item_map

    def add_skus_into_item_json(self):
        if not self.item_map:
            self.get_sku_and_title_mapping()
        logger.info("Matching our skus to our item json")
        for file in glob(os.path.join(self.output_dir, "*")):
            try:
                logger.info(f"=========================================")
                file_name = os.path.basename(file)
                logger.debug(f"Attempting to get the SKU for '{file_name}'")
                with open(os.path.join(file, 'item.json')) as infile:
                    item_json = json.load(infile)
                title = item_json.get("Title")
                sku = self.item_map.get(title, "")
                logger.debug(f"The sku we found: {sku}")
                item_json['SKU'] = sku
                with open(os.path.join(file, "item.json"), "w") as outfile:
                    outfile.write(json.dumps(item_json))
                # print(item_json['Title'].encode("utf8"), self.item_map.get(item_json['Title'].encode("utf8")))
            except Exception as e:
                logger.error(f"Fatal exception for {file}")
                logger.exception(e)

    def convert_json_to_csv(self):
        for i, file in enumerate(glob(os.path.join(self.output_dir, "*"))):
            try:
                logger.info(f"=========================================")
                file_name = os.path.basename(file)
                logger.debug(f"Attempting to get convert this guy to csv: '{file_name}'")
                with open(os.path.join(file, 'item.json')) as infile:
                    item_json = json.load(infile)

                with open(os.path.join(here, f"items.csv"), "a") as outfile:
                    if i == 0:
                        logger.info("Initial loop, writing our header row")
                        outfile.write(f"{','.join(item_json.keys())}\n")
                        # html_outfile.write()

                    outfile.write(f"{','.join(item_json.values())}\n")
                # print(item_json['Title'].encode("utf8"), self.item_map.get(item_json['Title'].encode("utf8")))
            except Exception as e:
                logger.error(f"Fatal exception for {file}")
                logger.exception(e)


if __name__ == "__main__":
    fancy_boi = OperationEbayEvacuation(download_images=True)
    fancy_boi.run()

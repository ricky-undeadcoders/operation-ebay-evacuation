#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from glob import glob

print("Number of input files:", len(glob("./input/*")))
print("Number of output files", len(glob("./output/*/item.json")))
